from datetime import datetime

from credit import Credit
from credit_db import CreditDb
from util import get_future_datetime_utc


class CreditLedger():

  def __init__(self, db_inst: CreditDb):
    self.db = db_inst

  def add_user_credit(self, user_id: int, amount: float, expires_at: datetime = None, created_at: datetime = None):
    if amount < 0:
      balance = self.get_user_balance(user_id)
      if balance + amount < 0:
        raise Exception('Not permitted - credits will drop below zero')
    new_credit = Credit(user_id, amount, expires_at=expires_at, created_at=created_at)
    self.db.save(new_credit)

  def get_user_balance(self, user_id: int, in_days: int = 0) -> float:
    balance = 0
    all_credits = self.db.get_all().values()
    at_time = get_future_datetime_utc(in_days)
    for credit in all_credits:
      if credit.user_id != user_id:
        continue
      if credit.expires_at and credit.expires_at <= at_time:
        continue
      balance += credit.amount
    return balance
