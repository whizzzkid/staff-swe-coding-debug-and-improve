# staff-swe-coding-debug-and-improve

Ledger of earning and consuming credits.

SnapTravel credits are earned by customers when they book certain hotel deals. Credits can be applied
to future bookings to reduce the price. We keep track of credits in a ledger, where each entry corresponds to a single
instance of a user earning or consuming credits.

Each credit is tied to one specific user (identified by a user_id) and has a certain amount. A positive amount
indicates the customer has earned the credit. A negative amount indicates the customer consumed credit they previously
earned. A credit is only available to be consumed if the current time is before it's expiry time. Credit consumptions do not
expire, only credits earned have an expiry.

Similar to a bank account, a user's credit balance is a sum of the credit amounts in the ledger, after taking into account
expires_at for each earned credit (after a credit expires, the unusued portion of that credit is no longer available to use).
The user's credit balance shouldn't ever go below zero. To support this, a new consumption credit (amount < 0) can only be
added to the ledger if the user has at least that amount of credits at that point in time.

In the scenario for this interview, one year after a user has not completed any credit transactions we have received reports from the 
user that they have observed their credit balance is negative. This scenario can be seen by running `python3 app.py`.

1. Please debug and discuss why this negative balance is occuring
2. Write a new test to reproduce the negative balance.
3. Discuss the problem with the interviedwer and possible solutions.
4. Implement a solution to fix the issue.
5. Verify the solution works by ensuring the test passes.

To run the script for the scenario:

`git clone https://gitlab.com/snaptravel/staff-swe-coding-debug-and-improve.git`

`cd staff-swe-coding-debug-and-improve`

`pip3 install -r requirements.txt --user`

`python3 app.py`

To run tests:

`python3 -m unittest discover tests`
