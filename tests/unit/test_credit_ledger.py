import unittest

from app import seed
from credit_db import CreditDb
from credit_ledger import CreditLedger


class TestApp(unittest.TestCase):

  def setUp(self):
    self.credit_db_inst = CreditDb()
    self.ledger = CreditLedger(self.credit_db_inst)
    seed(self.ledger)

  def test_get_user_credits(self):
    result = self.ledger.get_user_balance(user_id=1, in_days=31)
    self.assertEqual(5, result)
